class Coordinate:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def get_coordinate(self):
        return self.x, self.y
